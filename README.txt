## GDP Lifeguide - 2013 University of Southampton

This a self contained module developed for allocating participants in Randomised Control Trials.

IMPORTANT: Works with java 7+.

### Installation

Either use the pre compiled executable gdp-lifeguide.jar directly or download the whole project and build with maven.

### Usage

For all the instructions of how to use the module or any examples please refer to the 'wiki' file.

### For developers

The project is open source under the BSD License to University of Southampton.
The 'documentation' folder you can find the java documentation.
The 'src' folder contains all the source code and test code.
The 'res' folder contains any additional resources.
The project is developed using and Maven and the pom.xml contains all the information you would need in order to build.
The project report is located in the root folder as gdp_report.pdf.

