package ac.soton.gdp.lifeguide.app;

import java.awt.Font;


public class AppFonts{

	public static final Font defaultFont = new Font("sansserif", Font.PLAIN, 12);
	public static final Font errorFont = new Font("sansserif", Font.BOLD, 11);
	public static final Font tooltipFont = new Font("sansserif", Font.ITALIC, 10);
	
}
