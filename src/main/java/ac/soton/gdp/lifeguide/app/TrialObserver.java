package ac.soton.gdp.lifeguide.app;


public interface TrialObserver{

	public void notify(String trialName, LocalDBConnector database);

}
